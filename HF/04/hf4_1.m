clear; close; clc;

load mulkr_mrct_input4.mat

slice = 10;
treshold = graythresh(IMG(:,:,15));

[x, y, z, d] = subvolume(IMG,[nan, nan, nan, nan, nan, slice]);

iss = isosurface(x, y, z, d, treshold);
isc = isocaps(x,y,z,d,treshold);

p = patch(iss, 'FaceColor', 'blue', 'EdgeColor', 'none');
patch(isc, 'FaceColor', 'interp', 'EdgeColor', 'none')
isonormals(x, y, z, d, p);

colormap(gray(100));
daspect([1,1,0.4]);
view(3);
camlight right;
camlight left;
lighting gouraud;