clear; close; clc;

load mulkr_mrct_input3.mat

proba = iradon(data(:,:,1),1:180);
back = zeros([size(proba), size(data,3)]);

for k = 1:size(data, 3)
    back(:,:,k) = iradon(data(:,:,k), 1:180);
end

% objektum: Suzanne, the Blender Monkey 