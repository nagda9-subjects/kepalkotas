%-----------------------------------------------------------------------
% Job saved on 14-Apr-2021 21:00:16 by cfg_util (rev $Rev: 7345 $)
% spm SPM - SPM12 (7487)
% cfg_basicio BasicIO - Unknown
%-----------------------------------------------------------------------
matlabbatch{1}.spm.spatial.preproc.channel.vols = {'D:\Studies\Képalkotás\HF\07\sub-1_T1w.nii,1'};
matlabbatch{1}.spm.spatial.preproc.channel.biasreg = 0.001;
matlabbatch{1}.spm.spatial.preproc.channel.biasfwhm = 60;
matlabbatch{1}.spm.spatial.preproc.channel.write = [0 1];
matlabbatch{1}.spm.spatial.preproc.tissue(1).tpm = {'D:\Studies\Képalkotás\Gyak\Programok\spm12\tpm\TPM.nii,1'};
matlabbatch{1}.spm.spatial.preproc.tissue(1).ngaus = 1;
matlabbatch{1}.spm.spatial.preproc.tissue(1).native = [1 0];
matlabbatch{1}.spm.spatial.preproc.tissue(1).warped = [0 0];
matlabbatch{1}.spm.spatial.preproc.tissue(2).tpm = {'D:\Studies\Képalkotás\Gyak\Programok\spm12\tpm\TPM.nii,2'};
matlabbatch{1}.spm.spatial.preproc.tissue(2).ngaus = 1;
matlabbatch{1}.spm.spatial.preproc.tissue(2).native = [1 0];
matlabbatch{1}.spm.spatial.preproc.tissue(2).warped = [0 0];
matlabbatch{1}.spm.spatial.preproc.tissue(3).tpm = {'D:\Studies\Képalkotás\Gyak\Programok\spm12\tpm\TPM.nii,3'};
matlabbatch{1}.spm.spatial.preproc.tissue(3).ngaus = 2;
matlabbatch{1}.spm.spatial.preproc.tissue(3).native = [1 0];
matlabbatch{1}.spm.spatial.preproc.tissue(3).warped = [0 0];
matlabbatch{1}.spm.spatial.preproc.tissue(4).tpm = {'D:\Studies\Képalkotás\Gyak\Programok\spm12\tpm\TPM.nii,4'};
matlabbatch{1}.spm.spatial.preproc.tissue(4).ngaus = 3;
matlabbatch{1}.spm.spatial.preproc.tissue(4).native = [1 0];
matlabbatch{1}.spm.spatial.preproc.tissue(4).warped = [0 0];
matlabbatch{1}.spm.spatial.preproc.tissue(5).tpm = {'D:\Studies\Képalkotás\Gyak\Programok\spm12\tpm\TPM.nii,5'};
matlabbatch{1}.spm.spatial.preproc.tissue(5).ngaus = 4;
matlabbatch{1}.spm.spatial.preproc.tissue(5).native = [1 0];
matlabbatch{1}.spm.spatial.preproc.tissue(5).warped = [0 0];
matlabbatch{1}.spm.spatial.preproc.tissue(6).tpm = {'D:\Studies\Képalkotás\Gyak\Programok\spm12\tpm\TPM.nii,6'};
matlabbatch{1}.spm.spatial.preproc.tissue(6).ngaus = 2;
matlabbatch{1}.spm.spatial.preproc.tissue(6).native = [0 0];
matlabbatch{1}.spm.spatial.preproc.tissue(6).warped = [0 0];
matlabbatch{1}.spm.spatial.preproc.warp.mrf = 1;
matlabbatch{1}.spm.spatial.preproc.warp.cleanup = 1;
matlabbatch{1}.spm.spatial.preproc.warp.reg = [0 0.001 0.5 0.05 0.2];
matlabbatch{1}.spm.spatial.preproc.warp.affreg = 'mni';
matlabbatch{1}.spm.spatial.preproc.warp.fwhm = 0;
matlabbatch{1}.spm.spatial.preproc.warp.samp = 3;
matlabbatch{1}.spm.spatial.preproc.warp.write = [0 1];
%%
matlabbatch{2}.spm.spatial.realign.estwrite.data = {
                                                    {
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,1'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,2'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,3'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,4'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,5'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,6'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,7'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,8'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,9'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,10'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,11'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,12'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,13'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,14'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,15'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,16'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,17'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,18'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,19'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,20'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,21'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,22'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,23'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,24'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,25'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,26'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,27'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,28'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,29'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,30'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,31'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,32'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,33'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,34'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,35'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,36'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,37'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,38'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,39'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,40'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,41'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,42'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,43'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,44'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,45'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,46'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,47'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,48'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,49'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,50'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,51'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,52'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,53'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,54'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,55'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,56'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,57'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,58'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,59'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,60'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,61'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,62'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,63'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,64'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,65'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,66'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,67'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,68'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,69'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,70'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,71'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,72'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,73'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,74'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,75'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,76'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,77'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,78'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,79'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,80'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,81'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,82'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,83'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,84'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,85'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,86'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,87'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,88'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,89'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,90'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,91'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,92'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,93'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,94'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,95'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,96'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,97'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,98'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,99'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,100'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,101'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,102'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,103'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,104'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,105'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,106'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,107'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,108'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,109'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,110'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,111'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,112'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,113'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,114'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,115'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,116'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,117'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,118'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,119'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,120'
                                                    'D:\Studies\Képalkotás\HF\07\bold.nii,121'
                                                    }
                                                    }';
%%
matlabbatch{2}.spm.spatial.realign.estwrite.eoptions.quality = 0.9;
matlabbatch{2}.spm.spatial.realign.estwrite.eoptions.sep = 4;
matlabbatch{2}.spm.spatial.realign.estwrite.eoptions.fwhm = 5;
matlabbatch{2}.spm.spatial.realign.estwrite.eoptions.rtm = 1;
matlabbatch{2}.spm.spatial.realign.estwrite.eoptions.interp = 2;
matlabbatch{2}.spm.spatial.realign.estwrite.eoptions.wrap = [0 0 0];
matlabbatch{2}.spm.spatial.realign.estwrite.eoptions.weight = '';
matlabbatch{2}.spm.spatial.realign.estwrite.roptions.which = [2 1];
matlabbatch{2}.spm.spatial.realign.estwrite.roptions.interp = 4;
matlabbatch{2}.spm.spatial.realign.estwrite.roptions.wrap = [0 0 0];
matlabbatch{2}.spm.spatial.realign.estwrite.roptions.mask = 1;
matlabbatch{2}.spm.spatial.realign.estwrite.roptions.prefix = 'r';
matlabbatch{3}.spm.spatial.normalise.write.subj(1).def(1) = cfg_dep('Segment: Forward Deformations', substruct('.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','fordef', '()',{':'}));
matlabbatch{3}.spm.spatial.normalise.write.subj(1).resample(1) = cfg_dep('Segment: Bias Corrected (1)', substruct('.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','channel', '()',{1}, '.','biascorr', '()',{':'}));
matlabbatch{3}.spm.spatial.normalise.write.subj(2).def(1) = cfg_dep('Segment: Forward Deformations', substruct('.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','fordef', '()',{':'}));
matlabbatch{3}.spm.spatial.normalise.write.subj(2).resample(1) = cfg_dep('Realign: Estimate & Reslice: Resliced Images (Sess 1)', substruct('.','val', '{}',{2}, '.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','sess', '()',{1}, '.','rfiles'));
matlabbatch{3}.spm.spatial.normalise.write.woptions.bb = [-78 -112 -70
                                                          78 76 85];
matlabbatch{3}.spm.spatial.normalise.write.woptions.vox = [2 2 2];
matlabbatch{3}.spm.spatial.normalise.write.woptions.interp = 4;
matlabbatch{3}.spm.spatial.normalise.write.woptions.prefix = 'w';
matlabbatch{4}.spm.spatial.smooth.data(1) = cfg_dep('Normalise: Write: Normalised Images (Subj 2)', substruct('.','val', '{}',{3}, '.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('()',{2}, '.','files'));
matlabbatch{4}.spm.spatial.smooth.fwhm = [8 8 8];
matlabbatch{4}.spm.spatial.smooth.dtype = 0;
matlabbatch{4}.spm.spatial.smooth.im = 0;
matlabbatch{4}.spm.spatial.smooth.prefix = 's';
matlabbatch{5}.spm.spatial.coreg.estimate.ref(1) = cfg_dep('Realign: Estimate & Reslice: Mean Image', substruct('.','val', '{}',{2}, '.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','rmean'));
matlabbatch{5}.spm.spatial.coreg.estimate.source(1) = cfg_dep('Normalise: Write: Normalised Images (Subj 1)', substruct('.','val', '{}',{3}, '.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('()',{1}, '.','files'));
matlabbatch{5}.spm.spatial.coreg.estimate.other(1) = cfg_dep('Smooth: Smoothed Images', substruct('.','val', '{}',{4}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','files'));
matlabbatch{5}.spm.spatial.coreg.estimate.eoptions.cost_fun = 'nmi';
matlabbatch{5}.spm.spatial.coreg.estimate.eoptions.sep = [4 2];
matlabbatch{5}.spm.spatial.coreg.estimate.eoptions.tol = [0.02 0.02 0.02 0.001 0.001 0.001 0.01 0.01 0.01 0.001 0.001 0.001];
matlabbatch{5}.spm.spatial.coreg.estimate.eoptions.fwhm = [7 7];
