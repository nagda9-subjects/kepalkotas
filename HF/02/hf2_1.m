clear; close; clc;

sinogram = load("mulkr_mrct_input1.mat");

figure;
imshow(sinogram.R, []);
title(size(sinogram.R, 2));

i = 1:size(sinogram.R, 2);
ir = iradon(sinogram.R, i);
figure;
imshow(ir, []);

% A gond az, hogy a 90 felvétel nem 1°-os különbségel készült, így az iradon használatakor sem azt kell használni, mert akkor nem lesz jó a visszaállítás 

i = 1:2:180;
ir = iradon(sinogram.R, i);
figure;
imshow(ir, []);