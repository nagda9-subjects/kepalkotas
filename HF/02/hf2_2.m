clear; close; clc;

sinogram = load("mulkr_mrct_input1.mat");

figure;
imshow(sinogram.R, []);
title(size(sinogram.R, 2));

sinogram2 = zeros(size(sinogram.R, 1), 180);
for k = 1:90
    sinogram2(:,2*k-1) = sinogram.R(:, k);
end

i = 1:180;
ir = iradon(sinogram2, i);
figure;
imshow(ir, []);
title("0-kal feltöltve");

% Az első gondolatom az eljárás javítására a számtani közép volt. 

sinogram3 = sinogram2;
for k = 1:90
    if (k ~= 90) 
        sinogram3(:, 2*k) = round((sinogram.R(:, k) + sinogram.R(:, k+1)) / 2); 
    end
end

i = 1:180;
ir = iradon(sinogram3, i);
figure;
imshow(ir, []);
title("számtani középpel feltöltve");


% De megpróbáltam megduplázni is az eredeti adatokat. A számtani középpel
% végzett eljárás elméletileg jobb, de gyakorlatilag nem látszik nagy
% különbség ezen a példán.

sinogram4 = sinogram2;

for k = 1:90
    sinogram4(:, 2*k) = sinogram.R(:, k); 
end

i = 1:180;
ir = iradon(sinogram3, i);
figure;
imshow(ir, []);
title("duplázott adatok");

