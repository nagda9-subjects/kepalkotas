clear;
close; 
clc;

load mulkr_mrct_input5.mat;
t = (1:1000)/1000;
L = length(t);

figure;
for k=1:4
    Y = fft(FID(:,:,k));
    P2 = abs(Y/L);
    P1 = P2(1:L/2)*2;
    
    subplot(4,1,k);
    plot(P1);
end