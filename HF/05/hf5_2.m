clear;
close;
clc;

load mulkr_mrct_input5.mat;
t = (1:1000)/1000;
L = length(t);

Amps = zeros([4, 4]); % 4 fáziskódoló lépés 4 frekvencián

figure;
for k=1:4
    Y = fft(FID(:,:,k));
    P2 = abs(Y/L);
    P1 = P2(1:L/2)*2;
    
    %subplot(4,1,k);
    %plot(P1);
    
    Amps(k, :) = P1([101, 201, 301, 401]);
end

O = zeros(4); % original matrix
phase = [1, 1, 1, 1; 
    1, 1, -1, -1; 
    1, -1, -1, 1;
    1, -1, 1, -1];
 
for k = 1:4 % végigmegyünk minden oszlopon
    A = Amps(:,k);
    O(:, k) = phase/A'; % osztás u.a. mint inverz szorzás
end

disp(O);