level = 30;

figure(1)
subplot(3,3,1), imshow(squeeze(DifT(:,:,level,1)),[]); title('Dxx');
subplot(3,3,2), imshow(squeeze(DifT(:,:,level,2)),[]); title('Dxy');
subplot(3,3,3), imshow(squeeze(DifT(:,:,level,3)),[]); title('Dxz');
subplot(3,3,5), imshow(squeeze(DifT(:,:,level,4)),[]); title('Dyy');
subplot(3,3,6), imshow(squeeze(DifT(:,:,level,5)),[]); title('Dyz');
subplot(3,3,9), imshow(squeeze(DifT(:,:,level,6)),[]); title('Dzz');
sgtitle('Diffusion Tensor Components');

figure(2)
imshow(FA(:,:,level),[]); hold on;
VectorPlotZ=squeeze(V_dir(:,:,level,1:2));
[VectorPlotX,VectorPlotY]=meshgrid(1:size(VectorPlotZ,1),1:size(VectorPlotZ,2));
quiver(VectorPlotX,VectorPlotY,VectorPlotZ(:,:,2),VectorPlotZ(:,:,1),5);
title('Fractional Anistropy, and Vector Field'); hold off;

figure(3)
imshow(ADC(:,:,level),[]);
title('Apparent Diffuse Coefficient');