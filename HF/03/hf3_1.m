clear; close; clc;

load("mulkr_mrct_input2.mat");

newIR = ifanbeam(R, 185, "FanSensorGeometry", "line", "FanSensorSpacing", 0.5, "FanRotationIncrement", 2);

figure;
imshow(newIR,[]);