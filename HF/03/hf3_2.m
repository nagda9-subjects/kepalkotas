clear; close; clc;

load("mulkr_mrct_input2.mat");

newR_linear = interp2(R,"linear");
newR_spline = interp2(R,"spline");

newIR_linear = ifanbeam(newR_linear, 2*185, "FanSensorGeometry", "line", "FanSensorSpacing", 0.5, "FanRotationIncrement", 2);
newIR_spline = ifanbeam(newR_spline, 2*185, "FanSensorGeometry", "line", "FanSensorSpacing", 0.5, "FanRotationIncrement", 2);

figure;
subplot(121);
imshow(newIR_linear,[]);
title("linear interpolated");
subplot(122);
imshow(newIR_spline,[]);
title("spline interpolated");


% Ha a szinogram függőleges ("érzékelők közötti") felbontása kisebb, 
% akkor a visszaállított kép minősége hogy változik?

% Ha a felbontás kisebb, vagyis az érzékelők ritkábban helyezkednek el,
% akkor a visszaállított kép felbontása is romlani fog. Ennek a
% kiküszöbölésére, vagyis a fizikai korlátok legyőzésére alkalmazhatjuk az
% interpolációt.