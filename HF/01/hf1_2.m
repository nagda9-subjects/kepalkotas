% Alakítsd törtszámos alakba
% 
% Futtass rajta a diffúziós szűrést (Paraméterek: ConductionMethod-exponential, NumberOfIterations-5)
% 
% Maszkold a képet úgy, hogy csak a 0.3 és 0.8 közötti intenzitású részek maradjanak meg
% 
% Jelenítsd meg egy ábrán
% 
% (Alakítsd fekete-fehérbe maszkolás előtt, ha nem működne)

clear; close; clc;

I = imread("img2.PNG");
I = rgb2gray(I);
I = im2double(I);
I = imdiffusefilt(I,"ConductionMethod","exponential","NumberOfIterations",5);

th = graythresh(I);
mask = I > 0.3 & I < 0.8;
I = mask.*I;

imshow(I);