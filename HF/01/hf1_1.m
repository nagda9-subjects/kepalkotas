% Konvertáld törtszámos formába.
% 
% Jelenítsd meg egy ábrán aminek a címe (title) a legnagyobb intenzitású pixel értéke.
% 
% (Esetleg alakítsd fekete-fehérre, ha nem működne)

clear; close; clc;

I = imread("img1.png");
I = rgb2gray(I);
I = im2double(I);

imshow(I);
title(max(I(:)));